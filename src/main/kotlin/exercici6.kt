import java.util.*

interface GymControlReader{
    fun nextId():String
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId(): String =         scanner.nextLine()

}

fun exercici6(){
    val scanner = Scanner(System.`in`)
    var userState: MutableSet<String> = mutableSetOf()
    var id : String
    repeat(8){
        id = GymControlManualReader().nextId()
        if(userState.contains(id)){
            println("$id Sortida")
            userState.remove(id)
        }else{
            println("$id Entrada")
            userState.add(id)
        }
    }

}