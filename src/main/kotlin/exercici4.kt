import java.util.*

abstract class Question(
    var question: String,
    var answer: String,
){
    var resolved = false
    lateinit var multipleChoice: List<String>

    open fun printQuestion(){
        println(question)

    }

    abstract fun askAnswer()
    abstract fun isCorrect(input: String):String

}

class FreeTextQuestion( question:String, answer:String): Question(question, answer){

    override fun askAnswer() {
        println("Answer:")
    }

    override fun isCorrect(input:String):String{
        if(input==answer){
            this.resolved = true
            return ("Correct")

        }else{
            return ("Wrong")
        }
    }

}

class MultipleChoiseQuestion(question:String, answer:String, multipleChoiceInput: List<String>):  Question(question, answer){
    init{
        multipleChoice=multipleChoiceInput
    }

    override fun printQuestion(){
        super.printQuestion()
        for(i in 0 .. multipleChoice.lastIndex){
            print("${i+1}.${multipleChoice[i]}  ")
        }
        println()
    }
    override fun askAnswer(){
        println("Answer with the number:")
    }

    override fun isCorrect(input: String):String {
        if(input==answer){
            return ("Correct")
        }else{
            return ("Wrong")
        }
    }
}

data class Quiz(var listQuestions:MutableList<Question>)

fun countCorrectAnswers(quiz:Quiz): Int{
    var count = 0
    for(i in quiz.listQuestions){
        if(i.resolved) count++
    }
    return(count)
}
fun exercici4(){
    val scanner = Scanner(System.`in`)
    val quizExercici4 = Quiz(mutableListOf(MultipleChoiseQuestion("A que se refieren las siglas ITB?", "1", mutableListOf("Institut Tecnològic de Barcelona", "Invernader Tactic de Badalona", "Iniciació a Terraplanistes Bananers")),
        FreeTextQuestion("Resultado de 1+1","2"),
        MultipleChoiseQuestion("A que grupo de vertebrados pertenecen las ballenas? ","2", mutableListOf("Peces", "Mamíferos", "Amfíbios")),
        FreeTextQuestion("Cuantos sentidos tenemos?","6")))
    for(i in 0..quizExercici4.listQuestions.lastIndex){
        quizExercici4.listQuestions[i].printQuestion()
        quizExercici4.listQuestions[i].askAnswer()
        println(quizExercici4.listQuestions[i].isCorrect(scanner.nextLine()))
    }
    println("You did ${countCorrectAnswers(quizExercici4)} answers correctly")

}