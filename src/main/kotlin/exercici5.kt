enum class Direction{
    FRONT, LEFT, RIGHT
}
interface CarSensors{
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}

class AutonomusCar(

):CarSensors{
    fun doNextNSteps(n :Int){
        repeat(n){
            if(isThereSomethingAt(Direction.FRONT)){
                go(Direction.FRONT)
            }else if(isThereSomethingAt(Direction.RIGHT)){
                go(Direction.RIGHT)
            }else if(isThereSomethingAt(Direction.LEFT)){
                go(Direction.LEFT)
            }else{
                stop()
            }
        }
    }

    override fun isThereSomethingAt(direction: Direction): Boolean {
        TODO("Not yet implemented")
    }

    override fun go(direction: Direction) {
        TODO("Not yet implemented")
    }

    override fun stop() {
        TODO("Not yet implemented")
    }


}
