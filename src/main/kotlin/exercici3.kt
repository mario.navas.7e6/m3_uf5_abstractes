import kotlin.properties.Delegates

abstract class Instrument{
    abstract fun makeSounds(repeat:Int)
}


class Triangle(var resound : Int):Instrument() {
    override fun makeSounds(repeat: Int) {
        repeat(repeat) {
            when (resound) {
                1 -> println("TINC")
                2 -> println("TIINC")
                3 -> println("TIIINC")
                4 -> println("TIIIINC")
                5 -> println("TIIIIINC")

            }
        }

    }
}
class Drump(var sound: String) : Instrument() {
    override fun makeSounds(repeat: Int) {
        repeat(repeat) {
            when (sound) {

                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U" -> println("TUUUM")
            }
        }
    }


}



fun exercici3() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}
