import java.util.*

enum class Rainbow(var color: String){
    RED("vermell"), ORANGE("taronja"), YELLOW("groc"), GREEN("verd"), BLUE("blau"), INDIGO("anyil"), VIOLET("violeta")
}

fun exercici2(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un color")
    val colorInput = scanner.next()
    for( i in 0..Rainbow.values().lastIndex){
        if(colorInput==Rainbow.values()[i].color){
            println("true")
            break
        }else if(i == Rainbow.values().size){
            println("false")
        }
    }
}
