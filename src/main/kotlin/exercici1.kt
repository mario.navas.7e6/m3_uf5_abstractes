import org.w3c.dom.Text

data class Student(
    var name : String,
    var textGrade: Grades
)
enum class Grades(){
    HUNGEDUP, APPOVED, GOOD, REMARKABLE, EXCELLENT
}

fun exercici1(){
    val studentRaul = Student("Raul", Grades.HUNGEDUP)
    val studentAgustina = Student("Agustina", Grades.REMARKABLE)

    println(studentRaul)
    print(studentAgustina)
}